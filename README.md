# Simple Rust Learning
This repository will contain ![Rust By Example](https://doc.rust-lang.org/stable/rust-by-example/) and ![The Rust Programming Language Book](https://doc.rust-lang.org/book/) notes and the small exercises that comes with it, as well as simple exercises that I find elsewhere.

**It should be noted that `rustup doc` will bring up the entirety of the books and documentation in the local browser**

## Hello World and Hello Cargo

* Rust file extension is `.rs` and underscores should always separate multiple word file names .e.g. `hello_world.rs`
	* In a file such as this, the `main` function is special, it **always runs first**
```rs
fn main() {
// println! calls Rust macro, which is different to a function
    println!("Hello, world!");
}
```

* To run compile this file (and only this file), `rustc file_name.rs` is called. and then `./file_name` runs the executable that is produced by `rustc`


* Cargo is Rust's built-in system and package manager, good for larger projects
```sh
# creates new directory called project_name
cargo new project_name
```
* Creates `Cargo.toml`, `src/`
    * `src/` is where `main.rs` and more files live
    * In `Cargo.toml` .e.g.
```toml
[package]
name = "hello_cargo"
version = "0.1.0"
authors = ["Your Name <you@example.com>"]
edition = "2018"

[dependencies]
crate_name # this is a package in Rust
```

* `cargo build` creates an executable in `./target/debug/name`
    * `cargo build --release` puts executable in `./target/release`, builds the code with all the optimisations so is good, which slows the compile time down, but would make the code run faster, so is used when 'releasing'
* `cargo run` runs the binary executable if it hasn't been modified, otherwise it rebuilds it and then runs
* `cargo check` checks that the code compiles but doesn't produce an executable

## Reference `guessing_game` here?

## Programming Concepts (Within Rust)

### Variables And Mutability

* By default, **variables and immutable** (*.i.e. once value is bound to a name, you can't change that value*)
```rs
// immutable
let x = 9

//  mutablem, therefore can be changed
let mut x = 5
```


### Variables vs. Constants

* **Cannot use `mut` with constants**, as they are always immutable
* Declared with `const` rather than `let`
* Constant type (see Data Types [insert link here]) **must always be annotated**
```rs
// conventionally in caps
const MAX_POINTS: u32 = 100_000; // can have underscores to improve readability
```

### Shadowing

* Can create a new variable with the same name as a previous variable .e.g.
    * This is overwriting the previous instance of `x` with another instance of `x`
```rs
fn main() {
    let x = 5; // 5

    let x = x + 1; // 6

    let x = x * 2; // 12
}
```

* Also can create variables with the same name but with a different data type, no overwriting takes place
```rs
let spaces = "    ";    // string
let spaces = spaces.len();  // number

// don't need to create spaces_num and spaces_str
```
* `mut` doesn't allow variable type change


### Data Types

#### Scalar Types
* Represents a **single** value
* **Integer Types:**
    * Number without a fractional component
    * Signed or unsigned .e.g. `i8` or `u8`
        * `i8` to `i128` (same with `u8`)
        * `isize` depends on the architecture of the computer (.e.g. 32 or 64 bit)
    * Data types can be written before or after the integers
```rs
let number: u32 = 42;
// or
let number = 42u32;
```

| Number literals | Example |
| :-------------- | :-------: |
| Decimal         | `98_222` (can use underscores for readability) |
| Hex             | `0xff` |
| Octal           | `0o77` |
| Binary          | `0b1111_0000`|
| Byte (`u8` only)| `b'A'` |

* **Floating Types:**
    * Basically anything with a decimal
    * `f32` and `f64` with the latter being the default .e.g.
```rs
fn main() {
    let x = 3.2; // f64

    let y: f32 = 2.0; // f32

}
```

* **Numeric Symbols:**
    * All are basically standard to other programming languages:
        * `+` → addition
        * `-` → subtraction
        * `*` → multiplication
        * `/` → division
        * `%` → modulus
* **Boolean Types:**
    * Basically `true` or `false`
```rs
let t = true;

let f: bool = false; // explicit type annotation
```
* **Literal Operators (subsection of Booleans):**
    * `&&` → AND
    * `||` → OR
    * `XOR` → Exclusive OR

* **Character Type:**
    * Four bytes in size
    * Uses `' '` instead of `" "` like in Strings
    * Can star letters, other alphabets, and even emojis
```rs
let thinking_face = '🤔'
```

#### Compound Types
* Can group multiple values into one type
* **Tuple Type:**
    * *Fixed length*, can hold different data types within
```rs
// () is optional
let tup: (i32, f64, u8) = (500, 6.4, 1);

// destructuring one tuple into three variables (x, y, z)
let (x, y, z) = tup;

// access elements in a tuple with tuple_name.location

let tup1: (i32, f64, u8) = (700, 1.5, 5);

let seven_hundred: i32 = tup1.0; // starts at 0
```

* **Arrays:**
    * *Fixed length (in Rust)*
        * If want to change size, will use `Vec` (Vectors)
    * Must be all the same data type
```rs
fn main() {
    // [integer type; number of elements in the array] ← optional
    let a: [i32; 5] = [1, 2, 3, 4, 5];

    // [value, how many of this value]
    let b = [3; 5]; // same as [3, 3, 3, 3, 3]

    // access elements from the stack (as opposed to a heap which Vectors use)
    let c: [i32; 5] = [2, 5, 8, 11, 14];

    // similar to tuples but uses `[]` instead of `.`
    let first = c[0]; // 2

    let third = c[2]; // 8

    // Rust shows out of bounds if try access an element that is not there, unlike other low level languages
    let chicken = c[15];
```

### Functions

* Use `snake_case` like Python
* Declared with `fn` .e.g.
```rs
fn another_function(){
}
```
* Rust doesn't care the order in which they are defined, **as long as they are defined**.

#### Function Parameters

* Within a function, if it has a parameter, the **parameter data type must always be declared.**
```rs
// one parameter
fn single_parameter(x: i32){
    println!("Value of x is: {}", x);
}

// more than one parameter
fn multiple_parameters(x: i32, y: i32){
    ....
}
```

#### Function Bodies: Statements and Expressions

* Within function bodies, there are statements and expressions:
    * **Statements** - perform some action and *do not return a value*
    * **Expressions** - evaluate to a resulting value and *do not require semicolons*
```rs
// statement
fn main(){
    let y: i32 = 6;
}

// this statement will break the compiler as it does not return a value
fn main(){
    let x: i32 = (let y: i32 = 6);
}

// expression
fn main(){
    let y: i32 = {
        let x: i32 = 3;
        x + 1 // no semicolon as is an expression
    }
}
```

#### Functions With Return Values

* A function that only gives a value
    * Equal to the final value of the expression
* Don't need to name them, but with the `->` in the function declare line
```rs
// very simple function with a return value
fn five() -> i32 {
    5
}
```

```rs
// more complicated example

// main function
fn main() {
    let x = plus_one(6);
    println!("The value of x is: {}", x);
}

// function with a return values
fn plus_one(x: i32) -> i32 {
    x + 1
}
```

### Control Flow

#### If Statements

* Syntax → `if condition variable { something }`
* Don't need brackets like in other languages
```rs
// if-else-if statements
if condition {
    ...
} else if condition1 {
    ...
} else if condition2 {
    ...
} else {
    ...
}

// using if in let statements
fn main() {
    let condition = true;
    // values must be compatible data types
    let number = if condition { value } else { value1 }

    println!("Value of number: {}", number);
}
```

#### Loops

* Using `loop` key word, **repeats a block of code forever**, unless explicitly told to stop (with a `break` key word)
```rs
// repeat a print statement forever
loop {
    println!("chicken");
    }

// return a value from loops
fn main() {
    let mut counter = 0;

    let result = loop {
        // has += in Rust
        counter += 1;

        if counter = 20 {
            break counter * 2;
        }
    }; // assign value to 'result', technically could be all on one line
}
```

* `while` key word is used to evalute a condition
```rs
fn main(){
    let mut number = 5;

    while number != 0 {
        println!("{}!", number);
        number -=1;
    }

    // only when number is 0
    println!("Go!");
}
```

* `for` key word can be used to loop through arrays etc
```rs
let a = [10, 20, 30];

for element in a.iter() {
    println!("Value: {}", element);
    }

// using the while loop example as above
fn main() {
    // (1..6), not inclusive the 6, so goes to 5
    for number in (1..6).rev() {
        println!("{}!", number);
    }
    println!("Go!");
}
```
