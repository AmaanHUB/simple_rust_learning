use rand::Rng; //Calls random number generator library thing
use std::io; //Calls i/o library
use std::cmp::Ordering; // Ordering is enum. variants are Less, Greater, and Equal, used to compare values

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1,101); //rand::thread_rng() gives particular random # generator, one that is seeded by the operating system. gen_range() inclusive on the lower bound but exclusive on the upper bound

    loop {
        println!("Input your guess: ");

        let mut guess = String::new(); //Assigns a mutable variable 'guess' that is bound to a new, empty instance of String

         io::stdin() //stdin function from the io module,  could be written as std::io::stdin if not doing 'use std::io'
        .read_line(&mut guess) // calls read_line method, places input into the string as argument. & is reference, see later
        .expect("Failed to read line"); //Error handling, see more in Chapter 9




         /*
          * shadows the guess value, allows one to convert between value types, don't need to create two separate values.
          * the u32 chooses the number type, an unsigned 32-bit number, parse() turns into the
          * number, and the : annotates the integer
          * .trim() eliminates any whitespace from the string
*/
         let guess: u32 = match guess.trim().parse() {
         Ok(num) => num, //If parse successful, returns Ok signal
          Err(_) => continue, //If not successful, produce Err output, _ is catch all value, match all Err values no matter what is in them, so will executre the continue which forces the program to go through the loop again
         };

         println!("You guessed: {}", guess); //{} placeholder

         match guess.cmp(&secret_number) {
             Ordering::Less => println!("Too small!"),
             Ordering::Greater => println!("Too large!"),
             Ordering::Equal => {
                 println!("You win!");
                 break;
             }
         }
    }
}


